<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <title>Using the application &mdash; Taxonome 1.5 documentation</title>
    
    <link rel="stylesheet" href="_static/default.css" type="text/css" />
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    
    <script type="text/javascript">
      var DOCUMENTATION_OPTIONS = {
        URL_ROOT:    './',
        VERSION:     '1.5',
        COLLAPSE_INDEX: false,
        FILE_SUFFIX: '.html',
        HAS_SOURCE:  true
      };
    </script>
    <script type="text/javascript" src="_static/jquery.js"></script>
    <script type="text/javascript" src="_static/underscore.js"></script>
    <script type="text/javascript" src="_static/doctools.js"></script>
    <link rel="top" title="Taxonome 1.5 documentation" href="index.html" />
    <link rel="next" title="Concepts" href="concepts.html" />
    <link rel="prev" title="Installing Taxonome" href="setup.html" /> 
  </head>
  <body>
    <div class="related">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="concepts.html" title="Concepts"
             accesskey="N">next</a> |</li>
        <li class="right" >
          <a href="setup.html" title="Installing Taxonome"
             accesskey="P">previous</a> |</li>
        <li><a href="index.html">Taxonome 1.5 documentation</a> &raquo;</li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body">
            
  <div class="section" id="using-the-application">
<h1>Using the application<a class="headerlink" href="#using-the-application" title="Permalink to this headline">¶</a></h1>
<img alt="A screenshot of the application" class="align-center" src="_images/gui_screenshot_annotated.png" />
<p>The <strong>list of datasets</strong> on the left shows the datasets you have loaded. Click
on a dataset to select it, or double click to rename it.</p>
<p>With a dataset selected, you can use the <strong>search box</strong> on the top right. It
accepts species names or wildcard searches, and searches synonyms as well as
accepted names. &#8220;Festuca*&#8221; will find any species which the dataset lists in the
genus <em>Festuca</em>, including species with a synonym in <em>Festuca</em>. The results
are displayed (by accepted name) in the panel below. When no search is active,
the same panel shows 10 example taxa from the selected dataset.</p>
<p>The <strong>taxon view</strong> panel in the bottom right shows a taxon that has been selected
in the search results/example taxa panel. It displays details loaded from the
dataset, such as synonyms or distribution.</p>
<div class="section" id="loading-datasets">
<h2>Loading datasets<a class="headerlink" href="#loading-datasets" title="Permalink to this headline">¶</a></h2>
<p>From the file menu, you can load datasets from CSV files, which are available
from many sources online. A dialog will ask you to select which columns contain
the species names and authorities. There are three different types of data that
you can load from CSV files:</p>
<ul class="simple">
<li><strong>Taxa</strong>: Each row refers to one taxon.</li>
<li><strong>Synonyms</strong>: Each row connects one name to the accepted name for that taxon.</li>
<li><strong>Individual records</strong>: Each row refers to a separate specimen, sighting,
population, or something similar that is assigned to a taxon. The same name
may occur on many rows.</li>
</ul>
<p>You can also save and load data in a format specific to Taxonome, called
JSONlines in the menu.</p>
<p>Taxonome can also fetch datasets over the web (&#8216;Fetch collection of taxa&#8217; in the
web services menu). At present, this only works with the <a class="reference external" href="http://www.ars-grin.gov/cgi-bin/npgs/html/tax_search.pl">USDA GRIN database</a>, but we hope to add
more services in the future.</p>
</div>
<div class="section" id="matching-names">
<h2>Matching names<a class="headerlink" href="#matching-names" title="Permalink to this headline">¶</a></h2>
<p>The core feature of Taxonome is matching species names to a given set
of names, accounting for synonyms and spelling differences. To start, you need
to have a dataset loaded for the target synonymy you want to use. For example,
you may download synonym data for a family from GRIN, as described above. Or see
<a class="reference internal" href="customreaders.html"><em>Reading specific data sources</em></a> for scripts to load some other databases.</p>
<p>Then, from the Taxa menu, select <em>Match taxa by name</em>. Ensure that the <em>Match to
names in</em> option points to your target synonymy. You have the option to match
taxa from a loaded dataset, or from a CSV file. Matching directly from a CSV
file is useful if you have a very large dataset: Taxonome can read it
row-by-row, rather than loading the whole file into memory. If you pick a
CSV file, the next screen will ask you to select the name and authority columns,
just like when loading a dataset from CSV.</p>
<p>The next screen has a number of options to control the name-matching process:</p>
<ul class="simple">
<li>How should subspecies be treated if the target synonymy does not contain a
matching subspecies? By default, only nominal subspecies (e.g. <em>Zea mays</em>
subsp. <em>mays</em>) can be matched to their parent species, but you can choose to
reject all unmatched subspecies, or match them all to a parent species.</li>
<li>Where there are multiple matching names (homonyms), the one (if it exists)
which the target synonymy considers an accepted name is typically the most
likely match. By default, Taxonome will select this automatically when matching
a plain name without an authority. It can be set to always automatically pick
the accepted name, or never to do so.</li>
<li>If a name only has one match, but the authority for the name does not match
that for the taxon, Taxonome will normally allow the match; this can be
turned off.</li>
<li>When a name has multiple matches, Taxonome can present you with the matches
to make a manual selection. This is off by default, but can be enabled here.
How many times it asks you to choose will depend on your data, but as an
example, matching some 700 grass names from <a class="reference external" href="http://data.gbif.org/">GBIF</a>
produced about 10 questions to answer manually.</li>
</ul>
<p>The final screen offers a number of options for output from the matching process.</p>
<ul class="simple">
<li><strong>Taxa data with new names</strong> will save the input taxa data (as CSV) along with
the names they were matched to. Taxa that could not be matched are not included.</li>
<li><strong>Name mappings</strong> saves a CSV file of the input names matched to the target
names, including input names which weren&#8217;t matched (with empty fields for the
target name).</li>
<li><strong>Full log of matching process</strong> saves a CSV file detailing the steps used to
match each name.</li>
<li>You can also choose to make a Taxonome dataset of the input taxa with the
matched names. This will store the taxa in memory, so you shouldn&#8217;t use
it with very large (multi-gigabyte) datasets.</li>
</ul>
<div class="section" id="checking-the-matched-names">
<h3>Checking the matched names<a class="headerlink" href="#checking-the-matched-names" title="Permalink to this headline">¶</a></h3>
<p>You should check the matches Taxonome finds. Turn on the <strong>Name mappings</strong>
output, and when the matching has finished, load the CSV file in a spreadsheet
application. The first two colums show the original name and authority, the next
two show the matched name and authority, and the fifth column shows the closeness
of any fuzzy matching done. The lowest numbers here are the least certain matches,
so you can sort using this column to find them.</p>
<p>If the matched name looks very different from the input, it has probably been
matched through a synonym.</p>
<img alt="Example name mappings output" class="align-center" src="_images/names_matched_annotated.png" />
<p>To get more detail about the steps Taxonome used, you can turn on the <strong>Full log
of matching process</strong> output. This produces a CSV file with several rows for
each input name, showing intermediate steps like matched synonyms.</p>
</div>
</div>
<div class="section" id="combining-datasets">
<h2>Combining datasets<a class="headerlink" href="#combining-datasets" title="Permalink to this headline">¶</a></h2>
<p>When you have a number of datasets loaded using a common set of names, such as
after matching them to a preferred synonymy, you can combine them. From the Taxa
menu, select &#8216;Combine datasets&#8217;.</p>
<p>You can add source datasets to one of two groups: &#8216;target&#8217; datasets and
&#8216;background&#8217; datasets. A taxon is included in the results if it is in any target
dataset, with information attached from all the datasets in use which have the
same taxon. Background datasets are used to add information, but their taxa are
not automatically included.</p>
<p>For species&#8217; distribution information, you can choose to get distribution data
from one dataset, or to combine distribution information from all the datasets
in use.</p>
</div>
</div>


          </div>
        </div>
      </div>
      <div class="sphinxsidebar">
        <div class="sphinxsidebarwrapper">
  <h3><a href="index.html">Table Of Contents</a></h3>
  <ul>
<li><a class="reference internal" href="#">Using the application</a><ul>
<li><a class="reference internal" href="#loading-datasets">Loading datasets</a></li>
<li><a class="reference internal" href="#matching-names">Matching names</a><ul>
<li><a class="reference internal" href="#checking-the-matched-names">Checking the matched names</a></li>
</ul>
</li>
<li><a class="reference internal" href="#combining-datasets">Combining datasets</a></li>
</ul>
</li>
</ul>

  <h4>Previous topic</h4>
  <p class="topless"><a href="setup.html"
                        title="previous chapter">Installing Taxonome</a></p>
  <h4>Next topic</h4>
  <p class="topless"><a href="concepts.html"
                        title="next chapter">Concepts</a></p>
  <h3>This Page</h3>
  <ul class="this-page-menu">
    <li><a href="_sources/gui_manual.txt"
           rel="nofollow">Show Source</a></li>
  </ul>
<div id="searchbox" style="display: none">
  <h3>Quick search</h3>
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" />
      <input type="submit" value="Go" />
      <input type="hidden" name="check_keywords" value="yes" />
      <input type="hidden" name="area" value="default" />
    </form>
    <p class="searchtip" style="font-size: 90%">
    Enter search terms or a module, class or function name.
    </p>
</div>
<script type="text/javascript">$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="concepts.html" title="Concepts"
             >next</a> |</li>
        <li class="right" >
          <a href="setup.html" title="Installing Taxonome"
             >previous</a> |</li>
        <li><a href="index.html">Taxonome 1.5 documentation</a> &raquo;</li> 
      </ul>
    </div>
    <div class="footer">
        &copy; Copyright 2011, Thomas Kluyver.
      Created using <a href="http://sphinx-doc.org/">Sphinx</a> 1.2.2.
    </div>
  </body>
</html>